﻿using System;
using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
using UnityEngine.UI;

namespace TestAssets
{
    /// <summary>
    /// To be attached to any object which would require a FxOverlayEffects (i.e. fruit moving from its position on screen
    /// to the score pane). Invokes OnOverlayEffectRequest with the required parameters (fruit sprite and on-screen-position),
    /// listened by a FXOverlayManager 
    /// </summary>
    public class FxOverlayAgent : MonoBehaviour
    {
        public void LSTR_EffectRequest()
        {
            PairTypeVector2 parameters = new PairTypeVector2();
            parameters.m_position = m_camera.WorldToScreenPoint(transform.position);
            parameters.m_type = m_thisType;
            parameters.m_sprite = m_spriteSource.sprite;
            OnOverlayEffectRequest.Launch(parameters);
        }


        private void Awake()
        {
            m_camera = Camera.main;
        }

        private Camera m_camera;

        [SerializeField] private SpriteRenderer m_spriteSource;
        [SerializeField] private FruitType m_thisType;
        [SerializeField] private PairTypeVector2ScriptableEvent OnOverlayEffectRequest;
    }
}
