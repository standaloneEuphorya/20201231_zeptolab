﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace TestAssets
{
    /// <summary>
    /// Handles the fruit movement to the score pane when picked. Note that is not the fruit itself but a replica
    /// in the canvas. When Init, receives the sprite of the fruit directly from the caller to set the visuals and
    /// the position (raycasted to the camera). Then it moves to the reference targetPosition.
    /// Note that the end of the cycle is handled HNDL_AfterAnimationActions method, called from the Explosion animation;
    /// It invokes OnTargetReached UnityEvent to perform any final action such as returning the object to the pool.
    /// </summary>
    public class FxOverlayItem : MonoBehaviour
    {
        public void Init(PairTypeVector2 requesterData, Vector2 targetPosition)
        {
            m_image.gameObject.SetActive(true);
            m_animator.gameObject.SetActive(false);
            m_image.sprite = requesterData.m_sprite;
            m_target = targetPosition;
            transform.position = requesterData.m_position;
            enabled = true;
            //m_animator.SetTrigger(TRIGGER_IDLE);
        }

        public void HNDL_AfterAnimationActions()
        {
            OnTargetReached.Invoke();
        }

        private void Update()
        {
            transform.position = Vector2.MoveTowards(transform.position, m_target, 50);
            if (transform.position.AsVector2() == m_target)
            {
                m_animator.gameObject.SetActive(true);
                m_image.gameObject.SetActive(false);
                m_animator.SetTrigger(TRIGGER_EXPLOSION);
                enabled = false;
            }
        }

        [SerializeField] private Animator m_animator;
        private Vector2 m_target;
        [SerializeField] private Image m_image;
        [SerializeField] private UnityEvent OnTargetReached;

        private static readonly int TRIGGER_EXPLOSION = Animator.StringToHash("Explosion");
        private static readonly int TRIGGER_IDLE = Animator.StringToHash("Idle");
    }
}