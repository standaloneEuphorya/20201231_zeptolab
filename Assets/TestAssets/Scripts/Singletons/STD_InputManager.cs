﻿using System;
using UnityEngine;

namespace TestAssets
{
    public class STD_InputManager : ILCanProvideInput
    {
        private void Update()
        {
            if (Input.touchCount == 0)
                return;

            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                OnMainInput?.Invoke();
                Debug.Log("Input Started");
            }
        }


        #region ILCanProvideInput
        public override event Action OnMainInput;
        #endregion
    }
}