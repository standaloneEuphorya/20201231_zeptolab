﻿using System;


namespace TestAssets
{
    /// <summary>
    /// This is used to handle UPdate and FixedUpdate calls from the defined FSM states. When entering a state which will
    /// require updates, the state update method is suscribed to this object OnUpdate event. In this way a single Update (or
    /// FixedUpdate) call will provoke all states updates trigger. Is definde as singleton as it will need to be access
    /// from multiple states and stay alive through the full lifecycle.
    /// </summary>
    public class FSM_EventProvider : Singleton<FSM_EventProvider>
    {
        private void Update()
        {
            OnUpdate?.Invoke();
        }

        private void FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }

        public event Action OnUpdate;
        public event Action OnFixedUpdate;
    }
}