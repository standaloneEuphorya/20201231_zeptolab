﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// To be attached to any component which might damage damageables entities. On any trigger looks for ILDamageReceiver
/// on the offending side and, if any, calls to his LSTR_ReceiveDamage method (so it stays decoupled from the damage
/// rules
/// </summary>
public class DamageDealer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        ILDamageReceiver receiver = other.GetComponent<ILDamageReceiver>();
        if (receiver != null)
        {
            receiver.LSTR_ReceiveDamage();
        }

    }
}
