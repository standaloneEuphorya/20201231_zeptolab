﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Interface-Like component to be attached to any object which might get damaged. Simply offers the method
/// LSTR_ReceiveDamaged to any DamageDealer-type component. When damage is received it calls SpecificDamageBehaviour
/// wich should be implemented in children with the effects of the damage (i.e. for a 'lose life-type' behaviour,
/// a child of this ILDamageReceiver would include any life-related variables such as currentLifes, decrease
/// that count and check if zero and invoke OnDamageReceived event for any external required action (e.g. call
/// a 'CharacterDamaged' animation handled by an external component. Again, with this use of UnityEvents, coupling
/// is kept at minimum levels.
/// </summary>
public abstract class ILDamageReceiver : MonoBehaviour
{
    public void LSTR_ReceiveDamage()
    {
        OnDamageReceived.Invoke();
        SpecificDamageBehaviour();
    }

    public abstract void SpecificDamageBehaviour();

    [SerializeField] private UnityEvent OnDamageReceived;

}
