﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
/// <summary>
/// A very simple implementation for the player character. As in this game player dies at any contact with damage-dealers
/// and player death is represented by the game in AfterGameScreen, the specific behaviour of receiving damage is performed
/// by sending GameManager to that state. As GameManager is defined as singleton that request is sent by a direct call
/// </summary>
public class DamageReceiver_VerySimple : ILDamageReceiver
{
    public override void SpecificDamageBehaviour()
    {
        GameManager.Instance.SM_GoToAfterGameScreen();   
    }
}
