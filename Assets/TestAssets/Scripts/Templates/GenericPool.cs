﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
/// <summary>
/// Implement a generic pool of objects. It assumes that all objects will be GameObject and will include IPoolable
/// implementations.
/// </summary>
[System.Serializable]
public class GenericPool : MonoBehaviour
{
    /// <summary>
    /// Returns an object from the pool. If the pool is empty then, as safety measure, a new object is instantiated
    /// to be used at that moment and then recycled into the pool
    /// </summary>
  
    public ILPoolable GetFromPool()
    {
        if (m_pool.Count == 0)
        {
            ILPoolable newPoolable = GameObject.Instantiate(m_poolableObject);
            newPoolable.transform.parent = transform;
            newPoolable.p_ownerPool = this;
            newPoolable.AwakeFromPool();
            
            return newPoolable;
        }

        ILPoolable recoveredEntity = m_pool[0];
        m_pool.RemoveAt(0);
        //ecoveredEntity.GetComponent<IPoolable>().AwakeFromPool();
        recoveredEntity.AwakeFromPool();
        return recoveredEntity;

    }

    /// <summary>
    /// Sends back the object into the pool. Note that IPoolable interface will implement any per-object required action
    /// </summary>
    /// <param name="entity"></param>
    public void BackToPool(ILPoolable entity)
    {        
        m_pool.Add(entity);
        entity.transform.SetParent(this.transform);
        entity.transform.localPosition = Vector3.zero;
        entity.SleepToPool();
    }

    private void Start()
    {
        foreach (ILPoolable item in m_pool)
        {
            item.SleepToPool();
        }
    }
    
    [SerializeField]
    ILPoolable m_poolableObject;
    [SerializeField]
    protected List<ILPoolable> m_pool = new List<ILPoolable>();
}
