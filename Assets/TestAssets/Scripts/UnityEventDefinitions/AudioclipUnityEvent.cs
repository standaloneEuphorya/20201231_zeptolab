﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Unity event with predefined type for the ScriptableObjectEvent system
/// </summary>
[System.Serializable]
public class AudioclipUnityEvent : UnityEvent<AudioClip>
{

}
