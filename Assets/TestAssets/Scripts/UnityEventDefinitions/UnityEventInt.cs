﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Unity event with predefined type for the ScriptableObjectEvent system
/// </summary>
namespace TestAssets
{
    [System.Serializable]
    public class UnityEventInt : UnityEvent<int>
    {

    }
}
