﻿using System;
using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// The listener is, at the core, a converter from Scriptable Events into Unity Events, relaying any incoming parameter
/// to the final handlers for them to perform any required action.
/// </summary>
/// <typeparam name="P">Parameter: any data incoming in the listened event</typeparam>
/// <typeparam name="SE">Scriptable Event: Instance of the listened Scriptable Event</typeparam>
/// <typeparam name="UE">Unity Event: Outupt event converted from the Scriptable Event. Final handlers subscribe to this event </typeparam>

[System.Serializable]
public class ScriptableEventListener <P, SE, UE> : MonoBehaviour, IScriptableEventListener<P> 
    where SE : BaseScriptalbeEvent<P> where UE : UnityEvent<P>
{
    private void Awake()
    {
        m_listenedEvent.SubscribeListener(this);
    }

    public void OnEventTriggered(P anyParameter)
    {
        if (m_muteListener)
            return;
        m_outboundEvent?.Invoke(anyParameter);
    }
    
    [SerializeField] private bool m_muteListener;
    public SE p_listenedEvent { get => m_listenedEvent;set => value = m_listenedEvent;}
    [SerializeField] private SE m_listenedEvent;
    [SerializeField] public UE m_outboundEvent;
}
