﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

namespace TestAssets
{
    public class IntRetIntScriptableEventListener 
        : ScriptableEventListener<Delegate_IntReturnsInt, IntRetIntScriptableEvent, IntReturnsIntUnityEvent>
    {

    }
}