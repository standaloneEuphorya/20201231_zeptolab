﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PairTypeVector2ScriptableEventListener : ScriptableEventListener <PairTypeVector2, PairTypeVector2ScriptableEvent, PairTypeVector2UnityEvent>
{

}
