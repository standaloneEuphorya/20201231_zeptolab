﻿using System.Collections;
using System.Collections.Generic;
using MyNamespace;
using UnityEngine;
using UnityEngine.Events;

public class EmptyScriptableEventListener 
    : ScriptableEventListener<Empty, EmptyScriptableEvent, EmptyUnityEvent> 
{

}
