﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class AudioclipScriptableEventListener 
    :ScriptableEventListener<AudioClip, AudioclipScriptableEvent, AudioclipUnityEvent>
{

}
