﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
/// <summary>
/// Datatype for FxOverlay event calls. Using this the event can carry the sprite of the caller and the position in screen
/// Note that m_type is legacy and not used anymore
/// </summary>
public class PairTypeVector2 
{
    public FruitType m_type;
    public Sprite m_sprite;
    public Vector2 m_position;
}
