﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
[CreateAssetMenu(fileName = "IntReturnsInt",menuName = "Scriptables/Events/int Method (int i)")]
public class IntRetIntScriptableEvent : BaseScriptalbeEvent<Delegate_IntReturnsInt>
{}

[System.Serializable]
public delegate int Delegate_IntReturnsInt(int value);
