﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

[CreateAssetMenu(fileName = "PairTypeVector2ScriptableEvent",menuName = "Scriptables/Events/PairTypeVector2ScriptableEvent")]
public class PairTypeVector2ScriptableEvent : BaseScriptalbeEvent<PairTypeVector2>
{

}
