﻿using TestAssets;
using UnityEngine;

[CreateAssetMenu(fileName = "IntEvent",menuName = "Scriptables/Events/Integer")]
public class IntScriptableEvent : BaseScriptalbeEvent<int>
{

}
