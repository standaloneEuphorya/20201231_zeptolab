﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

[CreateAssetMenu(fileName = "AudcioclipEvent",menuName = "Scriptables/Events/Audioclip")]
public class AudioclipScriptableEvent : BaseScriptalbeEvent<AudioClip>
{

}
