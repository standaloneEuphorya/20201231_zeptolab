﻿
using System.Collections.Generic;
using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// Scriptable event generic template. To be extended for any requeriment of event with specific parameters (T). At
    /// the core is a list of listeners wich will subscribe to the event itself. By defining the event as Scriptable
    /// is it possible to create and use them as Assets in the project; by using T as template is possible to extend
    /// the event to carry any class as parameter to it's listener
    /// </summary>
    public class BaseScriptalbeEvent <T> :ScriptableObject
    {
        public void Launch(T data)
        {
            if (m_muteEvent)
                return;
            foreach (IScriptableEventListener<T> item in m_eventListeners)
            {
                item.OnEventTriggered(data);
            }
        }

        public void SubscribeListener(IScriptableEventListener<T> listener)
        {
            if(!m_eventListeners.Contains(listener))
                m_eventListeners.Add(listener);
        }

        public void UnsubscribeListener(IScriptableEventListener<T> listener)
        {
            if (!m_eventListeners.Contains(listener))
            {
                m_eventListeners.Remove(listener);
            }
        }

        [SerializeField] private bool m_muteEvent;
        private List<IScriptableEventListener<T>> m_eventListeners = new List<IScriptableEventListener<T>>();
        
    }
}
