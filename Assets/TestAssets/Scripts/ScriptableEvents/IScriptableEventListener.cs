﻿namespace TestAssets
{
    /// <summary>
    /// Template for ScriptableObjectEvent listeners. To be extended for specific type of events
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IScriptableEventListener <T>
    {
        void OnEventTriggered(T anyParameter);
    }
}
