﻿using System;
using UnityEngine;

namespace TestAssets
{


    /// <summary>
    /// abstract class as interface-like to allow serialization in inspector. Prefix IL (Interface-Like) indicates this.
    /// We want to Make this serializable to allow different input setups in the GameManager from the inspector in
    /// GameManager.m_availableInputs
    /// </summary>
    public abstract class ILCanProvideInput : MonoBehaviour
    {
       public abstract event Action OnMainInput;
    }
}