﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Interface-Like component for any object which can go into and out from generic pools. Include UnityEvents to be invked
/// on both actions to perform any additional action required by other components in the game object.
/// </summary>
public class ILPoolable : MonoBehaviour
{
    /// <summary>
    /// This method must be called to send the object back into its owner pool
    /// </summary>
    public void SelfReturnToPool() 
    {
        p_ownerPool.BackToPool(this);
    }
    public void SleepToPool()
    {
        OnSleepToPool.Invoke();

        //transform.parent = p_ownerPool.transform;
        transform.SetParent(p_ownerPool.transform);
        gameObject.SetActive(false);
    }

    public void AwakeFromPool()
    {
        gameObject.SetActive(true);
        transform.SetParent(p_ownerPool.transform.parent, false);
        OnAwakeFromPool.Invoke();
    }

    public GenericPool p_ownerPool {get => m_ownerPool; set { m_ownerPool = value; } }

    //public FruitType p_type { get => m_Type; set => m_Type = value; }


    [SerializeField] private GenericPool m_ownerPool;
    //[SerializeField] private FruitType m_Type;
    
    [SerializeField] public UnityEvent OnAwakeFromPool;
    [SerializeField] public UnityEvent OnSleepToPool;
    
}
