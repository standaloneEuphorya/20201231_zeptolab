﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles a set of audio channels (AudioSources) cached in the list m_availableSources. The list works as a FIFO
/// set. The number of playable clips at any given moment is defined by the number of sources (e.g. AudioFxManager
/// includes 3 sources. If at any point an audio request is received with no available channels, the oldest sound will
/// stop and the channel will be used for the new request. There are two of this in the scene, a 3-channels manager
/// for the FX sounds and a single channel manager for the ambient music (as only a music clip is played at any
/// given time)
/// The class is designed to receive the NewAudioRequest calls from a ScriptableEventListener. The listener 'listens'
/// a specific AudioclipScriptableEvent (there are two of them defined, one for the BackgroundMusic manager and other for
/// the SoundFxManager and translate it into a AudioclipUnityEvent. HNDL_NewAudioRequest is subscribed to the UnityEvent
/// </summary>
public class AudioManager : MonoBehaviour
{
    /// <summary>
    /// Always take the first channel from the list, use it to play the sound and finally put it at the end of the list
    /// In this way the method always get channels in reverse order of use.
    /// </summary>
    /// <param name="clip">Clip to play (recovered from the incoming AudioScriptableEvent->AudioUnityEvent)</param>
    public void HNDL_NewAudioRequest(AudioClip clip)
    {
        AudioSource audioSource = m_availableSources[0]; 
        m_availableSources.RemoveAt(0);
        m_availableSources.Add(audioSource);
        audioSource.clip = clip;
        audioSource.Play();
    }

    [SerializeField] private List<AudioSource> m_availableSources;

}
