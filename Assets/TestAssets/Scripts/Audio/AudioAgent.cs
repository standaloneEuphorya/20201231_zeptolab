﻿
using UnityEngine;
/// <summary>
/// Performs Audio requests to AudioMangers y launching OnAudioRequest events. The instance of the event launched
/// will determine the target AudioManager. The clip to play will be send with the event. Is designed to be included
/// in any GameObject which might require to play audio clips and exposed methods to be called from such objects (usually
/// through UnityEvents defined in other components in the object to achieve minimum coupling).
/// --m_audioClip. Default sound to play by the audio agent.
/// --m_muted. if true, OnAudioRequest events will not be launched.
/// </summary>
public class AudioAgent : MonoBehaviour
{
    /// <summary>
    /// Will launch OnAudioRequest with the default sound.
    /// </summary>
    public void HNDL_RequestPlayAudio()
    {
        if (m_muted)
            return;
        OnAudioRequest.Launch(m_audioclip);
    }
    /// <summary>
    /// Wil launch OnAudioRequest event with the provided AudioClip. Useful for entities which migh need to play
    /// different sounds such as the player character. 
    /// </summary>
    /// <param name="dynamicClip"></param>
    public void HNDL_RequestPlayAudio(AudioClip dynamicClip)
    {
        if (m_muted)
            return;
        OnAudioRequest.Launch(dynamicClip);
    }

    public void HNDL_SetMuteStatus (bool newStatus)
    {
        m_muted = newStatus;
    }
    
    [SerializeField] private bool m_muted = false;
    [SerializeField] private AudioClip m_audioclip;
    [SerializeField] private AudioclipScriptableEvent OnAudioRequest;
}
