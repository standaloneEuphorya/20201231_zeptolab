﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

/// <summary>
/// Set of level rules for every level. 
/// </summary>
[CreateAssetMenu(fileName = "Level rules",menuName = "Scriptables/Settings/Level rules")]
public class LevelRules : ScriptableObject
{
    [Tooltip("Level duration in seconds")]
    public int m_levelTime;
    [Tooltip("Minimum time to respawn fruits at each position")]
    public float m_minSpawnTick; 
    [Tooltip("Maximum time to respawn fruits at each position")]
    public float m_maxSpawnTick; 
    [Tooltip("Audio clip to be played in the level")]
    public AudioClip m_levelClip;
    [Tooltip("On level start, % of having a pre-spawned fruit in each spawner")]
    [Range(0, 100)] public int m_initialChanceOfSomething; //initial chance of the
    [Tooltip("% of different kind of fruits appearing. The array is filled with types of fruit. Whenever a new fruit is requested a random cell is picked and an object of the indicated type is spawned. ")]
    public FruitType[] m_randomizer; //This works pretty much as a deck of cards with any number of cards and any different number of copies for each one. Is a trade off to achieve an intuitive and readable way to control the chances for each fruit to be spawned
    
    public LevelControllerBuilder m_level;
    
    
    [Header("settings for the level builder")]
    public float m_cellSize;
    public int m_sizeX;
    public int m_sizeY;
    public int m_centerGridX;
    public int m_centerGridY;
    
    
}
