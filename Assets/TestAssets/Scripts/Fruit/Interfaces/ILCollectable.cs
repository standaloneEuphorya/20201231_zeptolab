﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace TestAssets
{
    /// <summary>
    /// Interface-Like component for object that can be picked by the player. At the core is just an interface
    /// to be found but Collector components and a OnCollected UnityEvent to trigger any required behaviour
    /// in other components in the same game object (again, in this way we keep the ILCollectable behaviour decoupled
    /// from other components).
    /// </summary>
    [SelectionBase]
    public class ILCollectable : MonoBehaviour
    {
        /// <summary>
        /// Method called from collector components
        /// </summary>
        [ContextMenu("Simulate collection")]
        public void LSTR_PerformCollection ()
        {
            OnCollected.Invoke();

            gameObject.SetActive(false);
        }
        /// <summary>
        /// When the object lifetime ends this method is called. It disabled the object and calls the OnSelfDestroy event.
        /// Basicalyy it performs the same actions than LSTR_PerformCollection except for the ScoreAgent.HNDL_ProvideScore
        /// </summary>
        public void PerformSelfCollection()
        {
            OnSelfDestroy.Invoke();
            gameObject.SetActive(false);
        }
        
        //to be called when returning from pool, to reset the lifetime parameter (if any)
        public void LSTR_WarmUp()
        {
            m_expireTime = Time.time + m_lifetime;
        }
        
        private void Update()
        {
            m_timeToSelfHide = m_expireTime - Time.time;
            if (Time.time > m_expireTime)
                PerformSelfCollection();
        }

        private void Start()
        {
            if (m_lifetime == 0)
                enabled = false;
            else
            {
                enabled = true;
            }
        }

        [Header("Watchers")] [SerializeField] private float m_timeToSelfHide; 
        private float m_expireTime;
        public float m_lifetime = 0;
        [SerializeField] public UnityEvent OnSelfDestroy; //called when the object reaches the end of it's lifecycle. To trigger behaviours such as 'play bad sound'
        [SerializeField] public UnityEvent OnCollected;//event called when the object is picked by the user. To trigger addicional behaviours such as gaining points or playing 'good' sound or returning to pool
    }

}