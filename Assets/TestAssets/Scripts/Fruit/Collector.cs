﻿using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// On trigger enter, looks for any ILCollectable interface in the offending object and, if any, invokes its
    /// LSTR_PerformCollection method.
    /// </summary>
    public class Collector : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            ILCollectable foundCollectable = other.GetComponent<ILCollectable>();
            if (foundCollectable != null)
            {
                foundCollectable.LSTR_PerformCollection();
            }
        }
    }
}
