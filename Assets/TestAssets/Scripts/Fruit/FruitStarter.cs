﻿using System;
using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
/// <summary>
/// Part of the fruit GameObjevct. It set a specific animation for the fruit depending on the selected type. With this
/// is possible to build all fruits as variants of a common prefab which already includes animator and sprites.
/// </summary>
public class FruitStarter : MonoBehaviour
{
    public void HNDL_Setup()
    {
        switch (m_fruitType)
        {
            case FruitType.Banana:
                m_animator.SetTrigger(TRIGGER_BANANA);
                break;
            case FruitType.Apple:
                m_animator.SetTrigger(TRIGGER_APPLE);
                break;
            case FruitType.Cherry:
                m_animator.SetTrigger(TRIGGER_CHERRY);
                break;
                
        }
    }

    [SerializeField][Tooltip("Select the sprite of fruit to show")]
    private FruitType m_fruitType;
    
    private static readonly int TRIGGER_APPLE = Animator.StringToHash("Apple");
    private static readonly int TRIGGER_BANANA = Animator.StringToHash("Banana");
    private static readonly int TRIGGER_CHERRY = Animator.StringToHash("Cherry"); 
    [SerializeField] private Animator m_animator;
}
