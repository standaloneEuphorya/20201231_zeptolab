﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

public class ScoreAgent_Value : ILScoreAgent
{

    protected override int CalculatedNewScore(int previousScore)
    {
        return previousScore + m_score;
    }
    
    [Header("Settings")] [Tooltip("Value to add when picked")] [SerializeField]
    private int m_score;


}
