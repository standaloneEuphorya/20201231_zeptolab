﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreAgent_Percent : ILScoreAgent
{
    protected override int CalculatedNewScore(int previousScore)
    {
        int scoreToAdd = previousScore * m_percent/100;
        return scoreToAdd + previousScore;
    }
    
    [Header("Settings")] [Tooltip("Percent of the current score to add when picked")] [SerializeField][Range(0,100)]
    private int m_percent;
}
