﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Similar to ILScoreAgent but it increases the time rather than the score. It uses a specific event listened by
/// GameManager
/// </summary>
public class TimeRewardAgent : MonoBehaviour
{
    public void HNDL_ProvideTime()
    {
        OnTimeRewardObtained.Launch(m_providedTime);
    }
    
    [SerializeField] private int m_providedTime = 5;
    [SerializeField] IntScriptableEvent OnTimeRewardObtained;
}
