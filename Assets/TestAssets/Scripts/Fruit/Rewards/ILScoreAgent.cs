﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Interface-Like component. To be attached to any object which might concede points. At the core, it provices an
/// abstract method CalculateNewScore to perform specific formula implementations for points (such as raw add a number
/// or a % value). HNDL_ProvideSocre (called from the entity deciding when to provide score (e.g., ILCollectable when
/// collected by subscribing to OnCollected event)). OnScoreObtained events carry a Delegate_IntReturnsInt parameter with
/// the reference to the calculation method. In this way the listener at the ScoreManager will receive the reference to the
/// method, feed it with the parameter and recover the calculated value. In this way maximum decoupling is achieved by
/// implementing the rules for the add-score formula in ILScoreAgent children but the calculation itself is performed at
/// ScoreManager, so the only flow is ILScoreAgent->ScoreManager.
/// </summary>
public  abstract class ILScoreAgent : MonoBehaviour
{
    public void HNDL_ProvideScore()
    {
        OnScoreObtained.Launch(CalculatedNewScore);
    }

    protected abstract int CalculatedNewScore(int value);

    //for any possible 'inside the prefab ¡' required actions
    public IntRetIntScriptableEvent OnScoreObtained;
}
