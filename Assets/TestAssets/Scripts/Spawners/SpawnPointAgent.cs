﻿using System;
using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
using Random = UnityEngine.Random;


namespace TestAssets
{
/// <summary>
/// Handles the fruit spawning
/// </summary>
    public class SpawnPointAgent : MonoBehaviour
    {
        /// <summary>
        /// Gets a fruit from its pool, put it in place in the surroundings of the spanwer and keep a reference of the fruit for later.
        /// Note that it suscribes HNDL_OwnedOllectableCollected to the fruit OnSleepToPool event so, when the fruit leaves the scene
        /// (either for being collected or by expiring), the spawner is notified and can start a countdown for
        /// a new fruit.
        /// </summary>
        /// <param name="isInitialSpawn">If true, only Apples are spawned. Otherwise a semi-random fruit is picked from the scene rules</param>
        public void SpawnPickable(bool isInitialSpawn)
        {
            FruitType typeToSpawn;
            if (isInitialSpawn)
                typeToSpawn = FruitType.Apple;
            else
            {
                int randomIndex = Random.Range(0, m_currentLevelRules.m_randomizer.Length);
                typeToSpawn = m_currentLevelRules.m_randomizer[randomIndex];
            }

            ILPoolable item = GameManager.Instance.p_fruitPools[typeToSpawn].GetFromPool();
            
            m_ownedCollectable = item.GetComponent<ILCollectable>();

            float cellSize = m_currentLevelRules.m_cellSize;
            float xOffset = Random.Range(-cellSize / 2, cellSize / 2);
            float yOffset = Random.Range(-cellSize / 2, cellSize / 2);
            Vector3 offset = new Vector3(Random.Range(-cellSize / 2, cellSize / 2), Random.Range(-cellSize / 2, cellSize / 2),0);
            Vector3 targetPosition = transform.position + offset;

            m_ownedCollectable.transform.position = targetPosition;
    
            
            //m_ownedCollectable.OnCollected.AddListener(HNDL_OwnedCollectableWasCollected);
            item.OnSleepToPool.AddListener(HNDL_OwnedCollectableWasCollected);
            Debug.DrawLine(transform.position, targetPosition, Color.yellow, 100);

            enabled = false;
        }

        public void HNDL_OwnedCollectableWasCollected()
        {
            //m_ownedCollectable.OnCollected.RemoveListener(HNDL_OwnedCollectableWasCollected);
            m_ownedCollectable.GetComponent<ILPoolable>().OnSleepToPool.RemoveListener(HNDL_OwnedCollectableWasCollected);
            m_ownedCollectable = null;

            StartCountdownToNextFruit();
        }

        /// <summary>
        /// When destroyed (because the level is ended), liberate any spawned fruit and send them to the owner pool.
        /// </summary>
        private void OnDestroy()
        {

            if (m_ownedCollectable != null)
            {
                m_ownedCollectable.OnCollected.RemoveListener(HNDL_OwnedCollectableWasCollected);
                m_ownedCollectable.GetComponent<ILPoolable>().SelfReturnToPool();
            }
        }

        private void Update()
        {
            if (Time.time > m_newFruitTime)
            {
                SpawnPickable(false);
                enabled = false;
            }
                
        }

        private void StartCountdownToNextFruit()
        {
            m_newFruitTime = Time.time + Random.Range(m_currentLevelRules.m_minSpawnTick, m_currentLevelRules.m_maxSpawnTick);
            enabled = true;
            if (!m_firstCallHappened)
                GameManager.Instance.OnGameStartDynamicEvent -= StartCountdownToNextFruit;
            
            m_firstCallHappened = true;
        }
        
        /// <summary>
        /// When the level is instantiated some fruits are generated (by chance set in level rules). Those who doesn't
        /// generate subscribe StartCountdownToNexFruit to OnGameStartDynamicEvent and thus,will wait without spawning
        /// anything till the event is launched (when the games go into InGame state)
        /// </summary>
        private void Start()
        {
            
            m_currentLevelRules = GameManager.Instance.m_availableLevels[GameManager.Instance.m_loadLevelIndex];
            int diceRoll = Random.Range(0, 100);
            if (diceRoll < m_currentLevelRules.m_initialChanceOfSomething)
            {
                SpawnPickable(true);
                m_firstCallHappened = true;
            }
            else
            {
                enabled = false;
                GameManager.Instance.OnGameStartDynamicEvent += StartCountdownToNextFruit;
            }
            // else
            // {
            //     StartCountdownToNextFruit();
            // }

            
        }

        private bool m_firstCallHappened = false;
        private float m_newFruitTime = 0;
        [SerializeField] private ILCollectable m_ownedCollectable;
        private LevelRules m_currentLevelRules;
    }
}
