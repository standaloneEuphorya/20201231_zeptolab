﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestAssets
{
    public class LevelControllerBuilder : MonoBehaviour
    {
        public void AddSpanwer(SpawnPointAgent newAgent)
        {
            newAgent.transform.parent = m_references.m_spawnersContainer;
        }


        public LevelController_References m_references;
    }
}
