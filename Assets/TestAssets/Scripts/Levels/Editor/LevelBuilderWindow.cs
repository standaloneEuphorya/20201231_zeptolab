﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace TestAssets
{
    public class LevelBuilderWindow : EditorWindow
    {

        [MenuItem("Test Tools/Level build helper")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            LevelBuilderWindow window = (LevelBuilderWindow) EditorWindow.GetWindow(typeof(LevelBuilderWindow));
            window.Show();
        }

        void OnGUI()
        {
            m_levelRules =
                EditorGUILayout.ObjectField(new GUIContent("Level rules", "Rules for the level. Must be pre-built"),
                    m_levelRules, typeof(LevelRules), true) as LevelRules;
            m_levelOnBuild =
                EditorGUILayout.ObjectField(
                    new GUIContent("Level on build", "Reference to the prefab of the level to setup"), m_levelOnBuild,
                    typeof(LevelControllerBuilder), true) as LevelControllerBuilder;
            m_spawnPointAgent =
                EditorGUILayout.ObjectField(
                    new GUIContent("Spawn point agent", "Object to spawn fruits at runtime in game"), m_spawnPointAgent,
                    typeof(SpawnPointAgent), true) as SpawnPointAgent;





            if (GUILayout.Button("Create matrix"))
            {
                BuildLevel();
            }

            if (GUILayout.Button(("set player ref")))
            {
                SetPlayerReference();
            }
        }

        void SetPlayerReference()
        {
            Vector3 position =
                m_levelOnBuild.m_references.m_levelGrid.GetCellCenterWorld(new Vector3Int(-m_levelRules.m_centerGridX,
                    -m_levelRules.m_centerGridY, 0));
            m_levelOnBuild.m_references.m_playerSpawnReference.transform.position = position;
        }

        void BuildLevel()
        {

            for (int yPos = 0; yPos < m_levelRules.m_sizeY; yPos++)
            {
                for (int xPos = 0; xPos < m_levelRules.m_sizeX; xPos++)
                {
                    Vector3 targetPosition = m_levelOnBuild.m_references.m_levelGrid.GetCellCenterWorld(
                        new Vector3Int(xPos - m_levelRules.m_centerGridX, yPos - m_levelRules.m_centerGridY, 0));
                    //SpawnPointAgent newSpawner = GameObject.Instantiate(m_spawnPointAgent);
                    SpawnPointAgent newSpawner = PrefabUtility.InstantiatePrefab(m_spawnPointAgent) as SpawnPointAgent;
                    newSpawner.name += "_" + xPos + "#" + yPos;
                    newSpawner.transform.position = targetPosition;
                    m_levelOnBuild.AddSpanwer(newSpawner);
                }
            }
        }


        private SpawnPointAgent m_spawnPointAgent;
        private LevelRules m_levelRules;
        private LevelControllerBuilder m_levelOnBuild;
    }
}