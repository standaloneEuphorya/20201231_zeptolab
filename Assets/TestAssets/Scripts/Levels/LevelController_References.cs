﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// references of the level object prefab. It offers references to the inner game objects
/// </summary>
[System.Serializable]
public class LevelController_References
{
    public Transform m_playerSpawnReference;
    public Grid m_levelGrid;
    public Transform m_spawnersContainer;
}
