﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using UnityEngine;


namespace TestAssets
{
    /// <summary>
    /// Handles the score state of the game. Note that this entity is decoupled from the GUIs and any required communication
    /// is performed by using events.
    /// </summary>
    public class ScoreManager : MonoBehaviour
    {
        /// <summary>
        /// Subscribed to the listener to IntRetInt event provided by ScoreAgents. Feeds the delegate with the current
        /// score and gets the updated score. Note that this manager doesn't care of the rules themselves, just call the
        /// method
        /// OnScoreChange event is listened by other entities (e.g. GUIs) to update the values whenever a change happens
        /// </summary>
        public void HNDL_ObtainScore(Delegate_IntReturnsInt calculateScore)
        {
            int scoreToAdd = calculateScore(p_currentScore);
            p_currentScore = scoreToAdd;

            OnScoreChange.Launch(p_currentScore);
        }
        /// <summary>
        /// Update the current level score by creating a unique playerprefs key.
        /// </summary>
        public void HNDL_UpdateBestScore()
        {
            string playerprefskey = PlayerprefsConsts.BEST_SCORE + GameManager.Instance.m_availableLevels[GameManager.Instance.m_loadLevelIndex];
            int savedScore = PlayerPrefs.GetInt(playerprefskey);
            if (savedScore < p_currentScore)
                PlayerPrefs.SetInt(playerprefskey, p_currentScore);
        }
       public void HNDL_RestartScoreCount()
        {
            p_currentScore = 0;
        }

        [ContextMenu("Reset saved score")]
        public void HNDL_ResetSavedScore()
        {
            PlayerPrefs.DeleteAll();
        }
        
        

        public int p_currentScore
        {
            get => m_currentScore;
            set => m_currentScore = value;
        }

        [Header("Watchers")] [SerializeField] private int m_currentScore;

        [Space(5)] [Header("Events")] [SerializeField]
        private IntScriptableEvent OnScoreChange;
    }
}
