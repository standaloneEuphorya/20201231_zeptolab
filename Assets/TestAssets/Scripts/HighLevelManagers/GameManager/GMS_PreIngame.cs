﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
/// <summary>
/// In this state the game will wait for the player to touch the screen and start playing. at the start of this state
/// the level to play is instantiated and set up
/// </summary>
public class GMS_PreIngame : GenericState<GameManager>
{
    public override void Enter()
    {
        base.Enter();
        m_target.m_references.OnEntersPreingame.Launch(new Empty());
        
        SelectAndInstantiateLevel();
    }

    public override void Exit()
    {
        base.Exit();
        m_target.m_references.OnExitsPreingame.Launch((new Empty()));
    }

    private void SelectAndInstantiateLevel()
    {
         m_target.m_loadLevelIndex = (int) Random.Range(0, m_target.m_availableLevels.Length);
         m_target.m_currentLevel = GameObject.Instantiate(m_target.m_availableLevels[m_target.m_loadLevelIndex].m_level);
         m_target.m_currentLevel.transform.position = Vector3.zero;

         m_target.m_references.OnMusicRequest.Launch(m_target.m_availableLevels[m_target.m_loadLevelIndex].m_levelClip);

         string playerprefskey = PlayerprefsConsts.BEST_SCORE + GameManager.Instance.m_availableLevels[GameManager.Instance.m_loadLevelIndex];
         int savedScore = PlayerPrefs.GetInt(playerprefskey);
  
         //int currentBestScore = int.Parse( );
         m_target.m_references.OnLevelWasSelected.Launch(savedScore);

    }
}
