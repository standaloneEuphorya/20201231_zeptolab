﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


namespace TestAssets
{
    /// <summary>
    /// This object is the game orchestrator and thus its presence is required by other lower-level entities. 
    /// </summary>
    public class GameManager : Singleton<GameManager>
    {
        #region state management
        /// <summary>
        /// Wrapper for state change methods
        /// </summary>
        [ContextMenu("Go to in main screen")]
        public void SM_GoToInMainScreen()
        {
            p_states.GoToState(typeof(GMS_InMainScreen));
        }
        [ContextMenu("Go to ingame")]
        public void SM_GoToInGame()
        {
            p_states.GoToState(typeof(GMS_InGame));
        }
        [ContextMenu("Go to after game")]
        public void SM_GoToAfterGameScreen()
        {
            p_states.GoToState(typeof(GMS_AfterGameScreen));
        }

        public void SM_GoToPreIngame()
        {
            p_states.GoToState(typeof(GMS_PreIngame));
        }
        #endregion
        
        /// <summary>
        /// Creates an instance of the selected input manager in the inspector. So far there is only one but
        /// this shows how different input origins can be organically added.
        /// </summary>
        private void InstantiateInputManager()
        {
            InputTypePair selectedPair = m_availableInputs.Find(pair => pair.m_type == m_selectedInput);
            p_activeInputController = GameObject.Instantiate(selectedPair.m_inputController);
        }

        /// <summary>
        /// To be called by the EventListener.OnTimeObtain and thus, add time to the level counter when required
        /// </summary>
        public void HNDL_ObtainTime(int time)
        {
            m_levelEndTime += time;
        }
        #region Unity callbacks
        private void Awake()
        {
            InstantiateInputManager();

        }

        private void Start()
        {
            p_states = new GameManager_FSM();
            p_states.AddState(new GMS_InMainScreen().Init(this));     
            p_states.AddState((new GMS_PreIngame()).Init(this));
            p_states.AddState(new GMS_InGame().Init(this));
            p_states.AddState(new GMS_AfterGameScreen().Init(this));

            p_fruitPools = new Dictionary<FruitType, GenericPool>();
            foreach (PoolTypePair item in m_references.m_availablePools)
            {
                p_fruitPools.Add(item.m_type, item.m_pool);
            }
        }

        #endregion

        public void EvtOnGameStartDynamicEvent()
        {
            OnGameStartDynamicEvent?.Invoke();
            
        }

        public GameManager_FSM p_states { get => m_states; set => m_states = value;}
        [Header("Watchers")] [SerializeField] public LevelControllerBuilder m_currentLevel;
        public int m_loadLevelIndex;
        [SerializeField] private GameManager_FSM m_states;
        [Header ("Settings")]
        [Space(5)][Tooltip ("Select a input controller to load at game start")]
        [SerializeField] private InputType m_selectedInput;
        [SerializeField] private List<InputTypePair> m_availableInputs;
        [SerializeField] public LevelRules[] m_availableLevels;


        /// <summary>
        /// Fruit pools are set in a list from the inspector. On start they are moved to a dictionary indexed by
        /// type for better runtime access.
        /// </summary>
        public Dictionary<FruitType,GenericPool> p_fruitPools { get; set; }
        
        public ILCanProvideInput p_activeInputController { get; private set; }
        public event Action OnGameStartDynamicEvent;    
        
        public float m_levelEndTime;

        public GameManager_References m_references;
        
    }
}
