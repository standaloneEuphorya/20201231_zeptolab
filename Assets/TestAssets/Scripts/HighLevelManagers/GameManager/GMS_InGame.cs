﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
/// <summary>
/// When entering this state (after touching the screen while in PreIngame state) an new random level is instantiated
/// and set up. Then level time countdown starts and is checked in Update; will move to AfterGameScreen state when
/// countdown ends. Note that is not the only flow to AfterGameScreen as such state can be reached when the player dies
/// </summary>
public class GMS_InGame : GenericState <GameManager>
{
    public override void Enter()
    {
        base.Enter();
        m_target.m_references.OnEntersIngame.Launch(new Empty());
        
        LoadSelectedLevel();

        m_target.EvtOnGameStartDynamicEvent();
        FSM_EventProvider.Instance.OnUpdate += Update;
    }

    public override void Update()
    {
        base.Update();
        
        int remainingTime = (int) (m_target.m_levelEndTime - Time.time);
        m_target.m_references.OnTimeTick.Launch(remainingTime);
        
        if (Time.time >= m_target.m_levelEndTime)
            m_target.SM_GoToAfterGameScreen();
    }

    public override void Exit()
    {
        base.Exit();
        m_target.m_references.OnExitsIngame.Launch(new Empty());
        
        FSM_EventProvider.Instance.OnUpdate -= Update;
    }

    private void LoadSelectedLevel()
    {
   
        LevelRules levelToLoad = m_target.m_availableLevels[m_target.m_loadLevelIndex];

        m_target.m_levelEndTime = Time.time + levelToLoad.m_levelTime;
    }



    
}
