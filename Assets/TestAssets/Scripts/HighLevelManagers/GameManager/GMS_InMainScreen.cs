﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
/// <summary>
/// The initial state of the main game cycle. Whenever is reached the existing instance of the level, if any,
/// is destroyed.
/// </summary>
public class GMS_InMainScreen : GenericState<GameManager>
{
    public override void Enter()
    {
        base.Enter();
            m_target.m_references.OnEntersMain.Launch(new Empty());

            if (m_target.m_currentLevel != null)
                GameObject.Destroy(m_target.m_currentLevel.gameObject);
    }

    public override void Exit()
    {
        base.Exit();
        m_target.m_references.OnExitsMain.Launch(new Empty());
    }
}
