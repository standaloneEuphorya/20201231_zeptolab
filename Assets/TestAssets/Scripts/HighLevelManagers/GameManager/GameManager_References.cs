﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameManager_References
{
    [Header ("Main game cycle events")]
    public EmptyScriptableEvent OnEntersMain;
    public EmptyScriptableEvent OnExitsMain;
    public EmptyScriptableEvent OnEntersPreingame;
    public EmptyScriptableEvent OnExitsPreingame;
    public EmptyScriptableEvent OnEntersIngame;
    public EmptyScriptableEvent OnExitsIngame;
    public EmptyScriptableEvent OnEntersAftergame;
    public EmptyScriptableEvent OnExitsAftergame;
    public IntScriptableEvent OnTimeTick;
    public AudioclipScriptableEvent OnMusicRequest;
    public IntScriptableEvent OnLevelWasSelected;
    [Header("non event objects")] public PoolTypePair[] m_availablePools;
}
