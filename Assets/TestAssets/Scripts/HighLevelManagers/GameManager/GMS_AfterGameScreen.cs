﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

public class GMS_AfterGameScreen : GenericState<GameManager>
{
    public override void Enter()
    {
        base.Enter();
        m_target.m_references.OnEntersAftergame.Launch(new Empty());
    }

    public override void Exit()
    {
        base.Exit();
        m_target.m_references.OnExitsAftergame.Launch(new Empty());
    }
}
