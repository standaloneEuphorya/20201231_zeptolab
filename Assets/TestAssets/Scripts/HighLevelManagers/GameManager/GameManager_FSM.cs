﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
/// <summary>
/// To serualize the FSM current state parameter
/// </summary>
[System.Serializable]
public class GameManager_FSM : Generic_FSM<GameManager>
{
}
