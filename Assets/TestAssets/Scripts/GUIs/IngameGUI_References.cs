﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class IngameGUI_References 
{
    public Text m_scoreText;
    public Text m_timeText;
}
