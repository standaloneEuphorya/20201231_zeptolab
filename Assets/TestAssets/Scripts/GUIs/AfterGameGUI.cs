﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Methods called by listeners to GameManger state change ScriptableObjectEvents
/// </summary>
public class AfterGameGUI : GameGUI
{
    public void HNDL_UpdateDisplayedScore(int score)
    {
        m_currentScoreText.SetText(':', score.ToString());
    }

    public void HNDL_UpdateDisplayedBestScore()
    {
        
        string playerprefskey = PlayerprefsConsts.BEST_SCORE + GameManager.Instance.m_availableLevels[GameManager.Instance.m_loadLevelIndex];
        m_bestScoreText.SetText(':', PlayerPrefs.GetInt(playerprefskey).ToString());
    }

    public void HNDL_GoToMainMenu()
    {
        GameManager.Instance.SM_GoToInMainScreen();
    }
    
    [SerializeField] private Text m_bestScoreText;
    [SerializeField] private Text m_currentScoreText;
}
