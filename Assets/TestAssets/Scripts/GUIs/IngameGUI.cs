﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Update the score and time pane with data from game manager. this is performed by listening ScriptableObjectEvents
/// </summary>
public class IngameGUI : GameGUI
{
    public void HNDL_SetUpdatedScore(int newScore)
    {
        m_references.m_scoreText.SetText(':', newScore.ToString());
    }

    public void HNDL_UpdateTimer(int newTime)
    {
        m_references.m_timeText.SetText(':', newTime.ToString());
    }
    
    [SerializeField] private IngameGUI_References m_references;
}


