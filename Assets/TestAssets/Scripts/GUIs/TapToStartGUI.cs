﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TestAssets
{
    public class TapToStartGUI : GameGUI
    {
        public void HNDL_SetBestScore(int score)
        {
            m_bestScoreText.SetText(':', score.ToString());

        }

        [SerializeField] private Text m_bestScoreText;
    }
}