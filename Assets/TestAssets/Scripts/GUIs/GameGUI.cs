﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// To be extended by more complex GUI objects (or used by itself). Just open/close methods and a AudioScriptableEvent
/// to be called when opened to perform request for playing sounds to the listener AudioManager
/// Open/Close methods are called from listeners subscribed to GameManager state events
/// </summary>
public class GameGUI : MonoBehaviour
{
    public virtual void HNDL_OpenGUI()
    {    
        gameObject.SetActive(true);
        if (m_thisGuiTheme!=null)
            OnBackgroundMusicRequest.Launch(m_thisGuiTheme);
    }

    public void HNDL_CloseGUI()
    {
        gameObject.SetActive(false);
    }

    public void Start()
    {
        if (m_hideOnStart)
            HNDL_CloseGUI();
    }

    [SerializeField] private AudioClip m_thisGuiTheme;
    [SerializeField] private AudioclipScriptableEvent OnBackgroundMusicRequest;
    [SerializeField] private bool m_hideOnStart = true;
}
