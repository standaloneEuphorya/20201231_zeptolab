﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

public class MainScreenGUI : GameGUI
{
    public void HNDL_StartNewGame()
    {
        GameManager.Instance.SM_GoToPreIngame();
    }

    public void HNDL_Quit()
    {
        Application.Quit();
    }

}
