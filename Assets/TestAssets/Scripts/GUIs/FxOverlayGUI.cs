﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// Handles the FX overlay layer (the 2d space where fruits moves from their position into the score pane). Is just
    /// an intermediary between requests from FxOverlayAgents ant the pool of FxOverlayItems. It works by listening
    /// a ScriptableObjectEvent OnOverlayFxRequest
    /// </summary>
    public class FxOverlayGUI : MonoBehaviour
    {
        public void HNDL_PickedItemFxRequest(PairTypeVector2 data)
        {
            Debug.Log("-- fx request happens --");
            ILPoolable recoveredItem = m_itemsPool.GetFromPool();
            FxOverlayItem fxItem = recoveredItem.GetComponent<FxOverlayItem>();
            fxItem.Init(data, m_targetPosition.position);



        }

        [SerializeField] private Transform m_targetPosition;
        [SerializeField] private GenericPool m_itemsPool;
    }
}
