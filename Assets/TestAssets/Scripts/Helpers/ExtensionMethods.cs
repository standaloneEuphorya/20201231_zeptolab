﻿using UnityEngine;
/// <summary>
/// Simple extension method to convert a Vector3 object into Vector2
/// </summary>
public static class ExtensionMethods
{
    public static Vector2 AsVector2(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.y);
    }
}
