﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour
{
    public void HNDL_DebugTest()
    {
        Debug.Log("--- test method called---");
        Debug.Log("--- test method called---");
        Debug.Log("--- test method called---");
    }

    
    [ContextMenu("Test grid")]
    public void HNDL_TestGrid()
    {
        Grid grid = GetComponent<Grid>();
        Vector3 initialPosition = transform.position;
        Vector3 targetPosition;
        targetPosition = grid.GetCellCenterLocal(new Vector3Int(-2, 0, 0));

        Debug.DrawRay(initialPosition, targetPosition, Color.cyan, 10);
    }
}
