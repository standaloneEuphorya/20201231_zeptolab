﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// Used to relay events to different entities. Usually used for animation events as the event subscriptor must
/// be in the same GameObject than the Animator and that can make things very messy. 
/// </summary>
public class EventsRelay : MonoBehaviour
{

    public void HNDL_RelayEvent()
    {
        m_eventRelay.Invoke();
    }
    [SerializeField] private UnityEvent m_eventRelay;
}
