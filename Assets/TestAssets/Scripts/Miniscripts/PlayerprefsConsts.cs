﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestAssets
{
    public static class PlayerprefsConsts
    {
        public const string BEST_SCORE = "BestScore";
    }
}
