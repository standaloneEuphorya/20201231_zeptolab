﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;

[System.Serializable]
public class PoolTypePair
{
    public FruitType m_type;
    public GenericPool m_pool;
}
