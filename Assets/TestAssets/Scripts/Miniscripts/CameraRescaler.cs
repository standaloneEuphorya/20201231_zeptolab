﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRescaler : MonoBehaviour
{

    public float halfWidth = 7f;
 
    void Start () 
    {

        Camera.main.orthographicSize = halfWidth / Camera.main.aspect;

    }
}
