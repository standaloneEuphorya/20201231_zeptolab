﻿using UnityEngine;
using UnityEngine.Serialization;

namespace TestAssets
{
    [System.Serializable]
    public class InputTypePair
    {
        public InputType m_type;
        public ILCanProvideInput m_inputController;
    }
}
