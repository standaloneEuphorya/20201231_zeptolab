﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Extensions 
{
    
    public static void SetText(this Text text, char separator, string incomingText)
    {
        text.text = text.text.Split(separator)[0] + ": " + incomingText;
    }
}
