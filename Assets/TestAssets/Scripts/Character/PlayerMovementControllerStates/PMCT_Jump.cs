﻿using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// This works as TRANSITION rather than as STATE as the player can go to InAir from running by falling or by
    /// performing a jump. For the latter is neccesary to apply a vertical force; such thing is done from here.
    /// Then goes to InAir. Any additional change (such as GoToClimbing) is handled by the InAir state
    /// </summary>
    public class PMCT_Jump : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();
            m_target.p_rigidbody.AddForce(Vector2.up * m_target.p_jumpForce, ForceMode2D.Impulse);
            
            m_target.SM_GoToInAir();
        }
    }
}
