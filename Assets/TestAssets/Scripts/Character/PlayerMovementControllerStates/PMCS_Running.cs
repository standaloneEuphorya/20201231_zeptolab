﻿

using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// The characer is running ina specific direction
    /// </summary>
    public class PMCS_Running : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();
            //Velocity is set whenever entering this state. Is sometimes redundant (i.e. when landing after a jump)
            //but might be necessary (when landing at zero horizontal speed as consequence of hitting a floating obstruction
            m_target.SetVelocity();
            m_target.m_eventReferences.OnStartRunning.Invoke();
            //Subscribes PerformJump state to OnMainInput (screen touched) event.            
            GameManager.Instance.p_activeInputController.OnMainInput += m_target.SM_PerformJump;
            FSM_EventProvider.Instance.OnFixedUpdate += FixedUpdate;

        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            //To guarantee that we get a vector with x component only and size +1/-1
            Vector2 direction = new Vector2(m_target.p_currentMovementDirection, 0);
            //while running the state checks for absence of obstructions in Down direction. If that happens means that the character moved through a slope
            //and must go to InAir state
            if (!m_target.CheckForObstruction(Vector2.down, 0, m_target.m_references.m_platformReferences, true, false)) 
                m_target.SM_GoToInAir();
        }

        public override void Exit()
        {
            base.Exit();
            GameManager.Instance.p_activeInputController.OnMainInput -= m_target.SM_PerformJump;
            FSM_EventProvider.Instance.OnFixedUpdate -= FixedUpdate;
        }
    }
}