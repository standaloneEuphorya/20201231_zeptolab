﻿
using TestAssets;


/// <summary>
/// This class is only to allow serialization of this FMS in the editor inspector for the MovementController object. In this
/// way we work around the Unity default serialization restrictions when dealing with generic templates.
///
/// MovementController_FMS type is IMoveable rather than PlayerMovementController to  achieve loose coupling
/// between these states and the entity they are controlling (so far MovementController but is easy to imagine
/// more entities such as IA controlled character not relying on player input and thus needing a specific controller
/// for the movement. With IMoveable, such controller could extend a specific version of IMoveable and thus reuse the
/// same set of states. Of course not every state would be reusable in that situation but at least those not relaying
/// on user input as InAir
///
/// In a nutshell, a IAMovementController (a movement controller for an IA rival competing with the player),
/// can be easily added from this base by:
///  --Create a new class MovementController which should include common methods from PlayerMovementController (such
/// as those needed to check if the player is on the floor or in contact with a wall) and move those methods from
/// PlayerMovementController to MovementController. PlayerMovementController should extend MovementController
///  --Implement IAMovementController extending MovementController. At this point, IAMovementController FSM
/// can reuse any suitable state designed for PlayerMovementController which doesn't need any player input (such as
/// PMCS_InAir).
///  --Implement non-compatible states for IAMovementController (probably to respond to events triggered from
/// a IA component) and add them to the states pool
///  --Implement specific SM_ methods in IAMovementController to target the right states 
 
/// </summary>
[System.Serializable]
public class PlayerMovementController_FSM : Generic_FSM<ILMoveable>
{

}
