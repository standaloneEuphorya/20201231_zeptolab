﻿

using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// The component is inactive
    /// </summary>
    public class PMCS_Disabled : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();

           
            m_target.p_rigidbody.simulated = false;
        }

        public override void Exit()
        {
            base.Exit();
            m_target.p_rigidbody.simulated = true;
        }
    }
}
