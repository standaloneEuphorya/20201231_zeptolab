﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestAssets
{

    public class PMCS_Idle : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();
            m_target.m_eventReferences.OnResetToIdle.Invoke();
            m_target.p_rigidbody.simulated = false;
            m_target.transform.position = m_target.m_initialPosition;
            GameManager.Instance.p_activeInputController.OnMainInput += m_target.SM_PerformInitialRun;
        }

        public override void Exit()
        {
            base.Exit();
            m_target.p_rigidbody.simulated = true;
            GameManager.Instance.p_activeInputController.OnMainInput -= m_target.SM_PerformInitialRun;
        }
    }
}
