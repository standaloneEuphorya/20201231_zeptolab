﻿
using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// The character is moving (up or down) in contact with a  wall
    /// </summary>
    public class PMCS_Climbing : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();
            m_target.m_eventReferences.OnClimbs.Invoke();
            
            FSM_EventProvider.Instance.OnFixedUpdate += FixedUpdate;
            GameManager.Instance.p_activeInputController.OnMainInput += HNDL_BouncingJump;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            //check if the climbed wall is still in touch (i.e. there is an obstruction in the direction of the movement). If not then goes to InAir
            if (!m_target.CheckForObstruction(Vector2.right * m_target.p_currentMovementDirection, m_target.m_references.m_collider.size.x, m_target.m_references.m_verticalAxisReferences, false, false))
            {
                m_target.SetVelocity();
                m_target.SM_GoToInAir();
                return;
            } 
            //check if the floor was reached (i.e. theere is an obstruction under the character). If so then go to Running states
            if (m_target.CheckForObstruction(Vector2.down, 0, m_target.m_references.m_platformReferences, true, true)) 
                m_target.SM_GoToRunning();
        }

        public override void Exit()
        {
            base.Exit();
            FSM_EventProvider.Instance.OnFixedUpdate -= FixedUpdate;
            GameManager.Instance.p_activeInputController.OnMainInput -= HNDL_BouncingJump;
        }


        /// <summary>
        /// Turn the direction of the character, the velocity and moves the FSM into Jumping state.
        /// As this behaviour is only triggered here it can be defined in the state. If at any moment any other state
        /// needs to call this method then it can be moved to the ILMoveable definition but, so far, is not needed.
        /// </summary>
        private void HNDL_BouncingJump ()
        {
            m_target.p_currentMovementDirection *= -1;
            m_target.SetVelocity();
            m_target.SM_PerformJump();
        }
    }
    
    
}