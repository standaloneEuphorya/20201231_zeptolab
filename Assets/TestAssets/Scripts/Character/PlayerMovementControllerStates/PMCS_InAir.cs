﻿using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// The character has jumped and is still in air (and not in touch with any wall)
    /// </summary>
    public class PMCS_InAir : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();
            m_target.m_eventReferences.OnJump.Invoke();
            
            FSM_EventProvider.Instance.OnUpdate += Update;
        }

        public override void Update()
        {
            base.Update();
            //check if touchs the ground. If so then go back to Running state
            if (m_target.CheckForObstruction(Vector2.down, 0, m_target.m_references.m_platformReferences, true, false))
            {
                m_target.SM_GoToRunning();
                return;
            }

            if (m_target.CheckForObstruction(Vector2.right * m_target.p_currentMovementDirection, m_target.m_references.m_collider.size.x, m_target.m_references.m_verticalAxisReferences, false, false))
            {
                m_target.SM_GoToClimbing();
                return;
            }
                
        }

        public override void Exit()
        {
            base.Exit();
            m_target.m_eventReferences.OnLands.Invoke();
            FSM_EventProvider.Instance.OnUpdate -= Update;
        }
    }
}