﻿

namespace TestAssets
{
    /// <summary>
    ///The character is in contact with a wall
    ///
    /// this state is not used anymore as development in Running and Climbing states made it redundant
    /// </summary>
    public class PMCS_TouchingWall : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();
            GameManager.Instance.p_activeInputController.OnMainInput += m_target.SM_PerformJump;
        }

        public override void Exit()
        {
            base.Exit();
            GameManager.Instance.p_activeInputController.OnMainInput -= m_target.SM_PerformJump;
        }
    }
}