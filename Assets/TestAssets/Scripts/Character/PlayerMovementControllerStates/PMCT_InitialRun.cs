﻿using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// Transition from the waiting state to running. Apply velocity in the initial direction and then moves
    /// to Running. Separated as specific transition to set the initial direction of the run.
    /// </summary>
    public class PMCT_InitialRun : GenericState<ILMoveable>
    {
        public override void Enter()
        {
            base.Enter();
            m_target.p_currentMovementDirection = m_target.p_initialDirection == Direction.Right ? m_target.p_currentMovementDirection = 1 : m_target.p_currentMovementDirection = -1;
            
            m_target.m_OnStartsMoving.Launch(new Empty());
            m_target.SM_GoToRunning();
        }
    }
}
