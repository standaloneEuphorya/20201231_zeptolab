﻿
using System;
using UnityEngine;

namespace TestAssets
{
    /// <summary>
    /// Extension of ILMoveable specifically build for the player character controller and control only character
    /// movements by listening InputManager events.
    /// </summary>
    public class PlayerMovementController : ILMoveable
    {

        #region ILMoveable States Management
        /// <summary>
        /// Override SM_ methods with the appropriate states for PlayerMovementController.
        /// </summary>
        public override void SM_GoToDisabled()
        {
           p_states.GoToState(typeof(PMCS_Disabled));
        }
        [ContextMenu("SM go to running")]
        public override void SM_GoToRunning()
        {
            p_states.GoToState(typeof(PMCS_Running));
        }
        public override void SM_GoToInAir()
        {
            p_states.GoToState(typeof(PMCS_InAir));
        }
        public override void SM_GoToTouchingWall()
        {
            p_states.GoToState(typeof(PMCS_TouchingWall));
        }
        public override void SM_GoToClimbing()
        {
            p_states.GoToState(typeof(PMCS_Climbing));

        }
        [ContextMenu("Perform jump")]
        public override void SM_PerformJump()
        {
            p_states.GoToState(typeof(PMCT_Jump));
        }
        [ContextMenu("Perform initial run")]
        public override void SM_PerformInitialRun()
        {
            p_states.GoToState(typeof(PMCT_InitialRun));
        }

        [ContextMenu("Go to idle")]
        public override void SM_GoToIdle()
        {
            p_states.GoToState(typeof(PMCS_Idle));
        }

        #endregion
        
        #region ILMoveable Helper methods
        /// <summary>
        /// Looks for potential object blocking the path. The objects layermask is defined in m_obstaclesLater. If origin
        /// are anchors then achor positions is used as reference for the raycast; if not then they are calculated from
        /// the center of the collider. The need for requiresFullContact and multiple origins arise from the fact that, when
        /// in Climbing state, one of the origins will ocassionally return true even before going into the floor (as walls
        /// colliders are composed by GridColliders). This provides a mechanism to distinguish that from actual contact
        /// with the floor 
        /// 
        /// </summary>
        /// <param name="direction">The direction to perform the search for blocking objects</param>
        /// <param name="baseRange">A value to add to the raycast range. Used to make the raycast go beyond the collider in this object</param>
        /// <param name="origins">Obstruction checks are performed from specific transforms provided here</param>
        /// <param name="requiresFullContact">if true will require to find blockers for every origin to return true (blocked).</param>
        /// <returns>True if any blocking object is present</returns>
        
        public override bool CheckForObstruction(Vector2 direction, float baseRange, Transform [] origins, bool originsAreAnchors, bool requiresFullContact)
        {
            //first check if x/y velocity are in the same direction than the check. if not then return always false 
            //This prevents logic errors such as 'detect collision with the floor right after jump, while going up,
            //in the brief moment where the ray still hits the floor
            if (p_rigidbody.velocity.y * direction.y < 0)
                return false;

            int currentContacts = 0;
            foreach (Transform item in origins)
            {
                Vector2 originPosition = (Vector2) (item.position.AsVector2());
                // if (!originsAreAnchors)
                //     originPosition += m_references.m_collider.offset;
                
                float range = (baseRange / 2) + m_obstacleDetectionTreshold;
                Debug.DrawRay(originPosition, direction * range, Color.red, .1f);

                RaycastHit2D hit = Physics2D.Raycast(originPosition, direction, range, m_obstaclesLayer);

        
                if (hit.collider != null)
                {
                    currentContacts++;
                }
            }

            if (currentContacts == origins.Length
                || (currentContacts > 0 && !requiresFullContact))
                return true;
            return false;
        }
        
        /// <summary>
        /// Sets the horizontal velocity for the movible object. Note that vertical velocity is
        /// reset to 0 when this method is called
        /// </summary>
        [ContextMenu("SetVelocity")]
        public override void SetVelocity()
        {
            p_rigidbody.velocity = Vector2.right * (p_currentMovementDirection * p_runSpeed);
            
            Debug.Log("---- Velocity set at ----->" +p_rigidbody.IsAwake() + p_rigidbody.velocity);
        }
        #endregion

        #region Unity callbacks
        
        private void Start()
        {
            m_initialPosition = transform.position;
            p_states = new PlayerMovementController_FSM();
            p_states.AddState(new PMCS_Disabled().Init(this));
            p_states.AddState(new PMCS_Running().Init(this));
            p_states.AddState(new PMCS_InAir().Init(this));
            p_states.AddState(new PMCS_Climbing().Init(this));
            p_states.AddState(new PMCT_Jump().Init(this));
            p_states.AddState(new PMCT_InitialRun().Init(this));
            p_states.AddState(new PMCS_Idle().Init(this));
        }
        #endregion

    
    }
}