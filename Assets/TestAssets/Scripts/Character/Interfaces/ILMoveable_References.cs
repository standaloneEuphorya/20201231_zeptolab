﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
///XYZ_References: _References classes works as static container for references to objects or components needed by
/// the XYZ object which are part of the same prefab/object who XYZ is part of (and thus, we can guarantee they
/// are not going to change and always be available)
/// This is done for a quick an easy 'fold' feature in the inspector which is very convenient as, once the references
/// are assigned, they are unlikely change and makes the inspector more readable by remaining folded.
/// As a container of references with no logic required 'the simplest, the best'.
/// </summary>
[System.Serializable]
public class ILMoveable_References
{
    public Rigidbody2D m_rigidbody;
    public BoxCollider2D m_collider;
    public Transform[] m_platformReferences;
    public Transform[] m_verticalAxisReferences;
    
}
