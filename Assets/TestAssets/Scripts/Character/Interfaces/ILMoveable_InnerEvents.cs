using TestAssets;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ILMoveable_InnerEvents
{
    /// <summary>
    /// As this events will be used only inside the prefab there is no particular need to use the events-as-scriptables
    /// system. Instead, common UnityEvents are good for this as we can set them from the inspector with minimum
    /// coupling.
    /// Usually invked from State.Enter methods and subscribed by other entities for things such as playing sounds
    /// or showing/hiding GUIs
    /// </summary>
    [Header("Inner Events")] [SerializeField] public UnityEventInt OnDirectionChange;

    [SerializeField] public UnityEvent OnStartRunning;
    [SerializeField] public UnityEvent OnJump;
    [SerializeField] public UnityEvent OnClimbs;
    [SerializeField] public UnityEvent OnResetToIdle;
    [SerializeField] public UnityEvent OnLands;
}