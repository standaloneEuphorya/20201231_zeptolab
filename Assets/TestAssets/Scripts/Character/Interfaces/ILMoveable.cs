﻿using System.Collections;
using System.Collections.Generic;
using TestAssets;
using UnityEngine;
using UnityEngine.Events;
/// ------------------------------------NOTE--------------------------------------------------------------- 
/// BY THE REQUIREMENTS, THIS ARCHITECTURE ADDS SOME ADDITIONAL COMPLEXITY FOR NO REAL GAIN. ALL OF THIS
/// COULD BE IMPLEMENTED DIRECTLY IN PlayerMovementController. IS  BUILT AS A DEMONSTRATOR OF THE CONCEPT.
/// ------------------------------------END OF NOTE--------------------------------------------------------

/// <summary>
/// Interface-like (IL goes for InterfaceLike). IL labels any component designed to be used as an interface. This approach
/// fits with Unity workflow of stacking components in a given Game Object, each one with a very specific behaviour. In a way
/// each of those components can be understood as an Interface from/where other objects can communicate with the owner
/// object.
/// In this project such interface-like components are defined as abstract or parent-root classes and then extended for specific implementations.
/// The  base class is, in a way, the 'interface' and any child class will be the class implementing the interface. In this
/// way, any child class can be always manipulated through the 'interface' and thus, fulfill the same role of a traditional
/// interface with all the advantages associated to Unity treatment for objects and components (such as Inspector serialization)
/// 
/// These components are in charge of small sets of responsibilities they usually don't need any additional splits but if the
/// need arises is always possible make them implement traditional interfaces to expose only specific subsets of the interface-like
/// object.
/// 
/// For entities with character-like movements. Declares all state
/// management methods needed plus additional headers for other methods needed to check the situation regarding
/// movement (check if in ground, check if wall reached and possibly others)
///


/// </summary>
public abstract class ILMoveable : MonoBehaviour
{
   /// All SM_ methods are wrappers for GenericFSM.GoToState for convenience. This creates some overhead when
   /// implementing  any statable class (as we could perform calls to p_states.GoToState from PlayerMovementController
   /// in this case) but it offers some level of protection as is not necessary to memorize the types of the states
   /// as, from the interface, only specific SM methods will ve available. This will create interfaces larger than usual
   /// but offers the capability of comunicating states with state owners via interfaces.
   #region state management
   public abstract void SM_GoToDisabled();
   public abstract void SM_GoToRunning();
   public abstract void SM_GoToInAir();
   public abstract void SM_GoToTouchingWall();
   public abstract void SM_GoToClimbing();
   public abstract void SM_PerformJump();
   public abstract void SM_PerformInitialRun();
   public abstract void SM_GoToIdle();
   #endregion
   
   #region Helper methods

   public abstract bool CheckForObstruction(Vector2 direction, float baseRange, Transform[] origins, bool originsAreAnchors, bool requiresFullContact);
   public abstract void SetVelocity();

   #endregion
   
   
   public PlayerMovementController_FSM p_states { get => m_states; set => m_states = value;}
   public float p_runSpeed { get => m_runSpeed; set => m_runSpeed = value; } 
   public float p_jumpForce { get => m_jumpForce; set => m_jumpForce = value;  }
   public Direction p_initialDirection { get => m_initialDirection; set => m_initialDirection = value;  }
   public Rigidbody2D p_rigidbody { get => m_references.m_rigidbody; set => m_references.m_rigidbody = value;}
   public int p_currentMovementDirection
   {
      get => m_currentMovementDirection;
      set
      {//any change of direction is followed by a OnDirectionChange event call. CharacterAnimationWrapper.HNDL_ChangeHeading
         //is subscribed to that event and thus performed on any direction change situation.
         m_currentMovementDirection = value;
         m_eventReferences.OnDirectionChange.Invoke(m_currentMovementDirection); 
      }
   }


   [Header ("Watchers")]   
   [SerializeField] private PlayerMovementController_FSM m_states;
   [Tooltip("-1 means from right to left; +1 from left to right. Internally works as multiplier to Vector2.right when setting speed so it needs to be updated when the direction changes")]
   [SerializeField] private int m_currentMovementDirection;
   
   [Header ("Settings")] [Space (5)]
   [SerializeField] protected LayerMask m_obstaclesLayer;
   [SerializeField] private float m_runSpeed;
   [SerializeField] private float m_jumpForce;
   [SerializeField] private Direction m_initialDirection;
   [SerializeField] public EmptyScriptableEvent m_OnStartsMoving; 
   
   [Space (5)]
   public ILMoveable_References m_references;
   public  ILMoveable_InnerEvents m_eventReferences;

   [HideInInspector] public  Vector3 m_initialPosition;
   

   public const float m_obstacleDetectionTreshold = .1f;
}
