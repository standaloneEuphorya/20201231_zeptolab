﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


/// <summary>
/// This class exposed methods as an interface for external entities. Is designed for character-like objects.
/// At this point is not needed to build it as a Interface-Like entity but can easily be rebuild to that form
/// if the need arises by implementing and abstract parent defining the common methods/properties (as was already
/// done for ILMoveable/PlayerMovementController.
///
/// Regarding animator calls, this class exposes specific per-toggle methods (in the animator) so any entity making calls
/// can abstract from the animator or any animation (or other visual) related stuff.
/// </summary>
public class CharactersAnimationsWrapper : MonoBehaviour
{
    #region animator calls wrapper
    public void HNDL_Run()
    {
        m_references.m_animator.SetTrigger(TRIGGER_RUNS);
    }
    
    public void HNDL_Airs()
    {
        m_references.m_animator.SetTrigger(TRIGGER_AIRS);
    }
    public void HNDL_Lands()
    {
        m_references.m_animator.SetTrigger(TRIGGER_LANDS);
    }
    public void HNDL_Climbs()
    {
        m_references.m_animator.SetTrigger(TRIGGER_CLIMBS);
    }

    public void HNDL_ResetToIdle()
    {
        m_references.m_animator.SetTrigger(TRIGGER_RESETTOIDLE);
    }
    #endregion
    /// <summary>
    /// Not an animation (as it changes the heading by scale * -1) but in concept is the sime kind of action
    /// so it's included here.
    /// </summary>
    /// <param name="newHeadingDirection"></param>
    public void HNDL_ChangeHeading(int newHeadingDirection)
    {
            m_references.m_spriteHolder.localScale = new Vector3(newHeadingDirection * m_initialScale.x, 
                                                            m_references.m_spriteHolder.localScale.y, 
                                                            m_references.m_spriteHolder.localScale.z);
    }
    

    private void Awake()
    {
        m_initialScale = m_references.m_spriteHolder.localScale;
    }

    [SerializeField] private CharacterAnimationsWrapper_References m_references;

    private Vector3 m_initialScale;
    
    private static readonly int TRIGGER_RUNS = Animator.StringToHash("Runs");
    private static readonly int TRIGGER_AIRS = Animator.StringToHash("Airs"); 
    private static readonly int TRIGGER_LANDS= Animator.StringToHash("Lands"); 
    private static readonly int TRIGGER_CLIMBS = Animator.StringToHash("Climbs");
    private static readonly int TRIGGER_RESETTOIDLE = Animator.StringToHash("ResetToIdle"); 
}

/// <summary>
/// Set of references. Usually classes with the label _References, built in this way to provide a quick way to
/// fold sets of exposed variables in the inspector. Usually for variables which are set only once from the inspetor
/// and then never change.
/// </summary>
[System.Serializable]
public class CharacterAnimationsWrapper_References
{
    public Animator m_animator;
    public Transform m_spriteHolder;
}